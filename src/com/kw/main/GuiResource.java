package com.kw.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.graphics.Color;
import android.graphics.Point;

import com.example.graphics.R;
import com.kw.mw.MW;
import com.kw.mw.graphics.animator.AnmElm;
import com.kw.mw.graphics.animator.AnmElm.EndsBehavior;
import com.kw.mw.graphics.animator.AnmElm.Motion;
import com.kw.mw.graphics.animator.AnmElm.Target;
import com.kw.mw.graphics.animator.AnmElms;
import com.kw.mw.graphics.gui.GuiElm;
import com.kw.mw.graphics.gui.GuiElms;
import com.kw.mw.graphics.gui.GuiElm.*;

public class GuiResource {

	private static final int NA = MW.NA;

	public enum Gui
	{   
		RECT__PURE_BLUE(
			new GuiElm(Type.SHAPE, Attr.RECT, NA,     //type, attr, aux
					   Color.BLUE, Color.BLUE,        //fcolor, bcolor
					   Scale.NONE, NA,                //scale, strokeW
					   0,  0,  0, 200, 200,  NA)),    //GP, L, T, W, H, ResId
		RECT__YELLOW_BODY_RED_EDGE_STROKE_10PXL(
			new GuiElm(Type.SHAPE, Attr.RECT, NA,
					   Color.RED, Color.YELLOW, 
					   Scale.NONE, 10,
					   0, 100, 100, 200, 200,  NA)),
		OVAL__CIRCLE_BLACK_BODY_WHITE_EDGE_STROKE5(
			new GuiElm(Type.SHAPE, Attr.OVAL, NA,
					   Color.WHITE, Color.BLACK, 
					   Scale.NONE, 5,
					   1, 250, 250, 200, 200,  NA)),	 
		OVAL__ELLIPSE_PURE_RED(
			new GuiElm(Type.SHAPE, Attr.OVAL, NA,
					   Color.RED, Color.RED, 
					   Scale.NONE, NA,
					   1, 300, 400, 400, 200,  NA)),			   
		STRING__NORMAL_FONT_NO_OUTLINE(
			new GuiElm(Type.STRING, Attr.NORMAL, NA,
					   Color.LTGRAY, Color.YELLOW, 
					   Scale.NONE, 0,
					   2, 0, 500, 800, 300, R.string.test_string)),
		STRING__IMPACT_FONT_OUTLINE_3PXL(
			new GuiElm(Type.STRING, Attr.NORMAL, Font.IMPACT.s,
					   Color.LTGRAY, Color.YELLOW, 
					   Scale.NONE, 3,
					   2, 0, 800, 800, 100, R.string.test_string)),						   
		BMP__SCALE_NONE(
			new GuiElm(Type.BITMAP, Attr.STATIC, NA,
					   NA, NA, 
					   Scale.NONE, 0,
					   3, 100, 0, 300, 300, R.drawable.ic_launcher)),
		BMP__SCALE_NONE_ROTATE_45(
			new GuiElm(Type.BITMAP, Attr.STATIC, NA,
					   NA, NA, 
					   Scale.FIT_RECT, 0,
					   3, 100, 0, 300, 300, R.drawable.ic_launcher)),
		BMP__SCALE_FIT_RECT_ROTATE_45(
			new GuiElm(Type.BITMAP, Attr.STATIC, NA,
					   NA, NA, 
					   Scale.FIT_RECT, 45,
					   3, 300, 100, 300, 300, R.drawable.ic_launcher)),
		LINES__TRIANGLE_YELLOW_STROKE_3PXL(
			new GuiElm(Type.SHAPE, Attr.LINES, PointSeqs.TRIANGLE.s,
					   Color.YELLOW, Color.YELLOW, 
					   Scale.NONE, 3,
					   4, 600, 0, 100, 100,  NA)),
		LINES__SQUARE_WHITE_STROKE_1PXL(
			new GuiElm(Type.SHAPE, Attr.LINES, PointSeqs.SQUARE.s,
					   Color.WHITE, Color.WHITE, 
					   Scale.NONE, 1,
					   4, 600, 200, 100, 100,  NA));
		
		public final GuiElm id;
		
		private Gui(GuiElm gobj)
		{
			this.id = gobj;
		}
	}
	
	public enum Anm
	{
		BMP_ALPHA(
				new AnmElm(Gui.BMP__SCALE_NONE.id, 
						   Target.ALPHA, Motion.LINEAR_LINEAR, EndsBehavior.AUTO_SHOW_HIDE, 
						   AnmElm.LOOP_INFINITE, 3000)), // loop_no, duration
		BMP_FLY(
				new AnmElm(Gui.BMP__SCALE_NONE.id, 
						   Target.POSITION, Motion.LINEAR_LINEAR, EndsBehavior.AUTO_SHOW_HIDE, 
						   AnmElm.LOOP_INFINITE, 3000)), // loop_no, duration
		SQUARE_FLY(
				new AnmElm(Gui.RECT__PURE_BLUE.id, 
						   Target.SIZE, Motion.LINEAR_LINEAR, EndsBehavior.AUTO_SHOW_HIDE, 
						   AnmElm.LOOP_INFINITE, 3000));
		
		public final AnmElm id;
		
		private Anm(AnmElm anm)
		{
			this.id = anm;
		}
	}
	
	public enum Guis
	{
		BULLETS(
			new GuiElms(10, Type.SHAPE, Attr.OVAL, NA,
						    Color.RED, Color.YELLOW, 
						    Scale.NONE, 2,
						    1, 0, 0, 30, 30, NA).get());
		
		public final ArrayList<GuiElm> id;
		
		private Guis(ArrayList<GuiElm> gobjs)
		{
			this.id = gobjs;
		}
	}
	
	public enum Anms
	{
		BULLETS(
			new AnmElms(10, Guis.BULLETS.id, 
					        Target.POSITION, Motion.LINEAR, EndsBehavior.AUTO_SHOW_HIDE, 
					        1, 10000).get());
		
		public final ArrayList<AnmElm> id;
		
		private Anms(ArrayList<AnmElm> anms)
		{
			this.id = anms;
		}
	}
		
	public enum Font
	{
		IMPACT("fonts/impact.ttf");
		
		public final String s;
		
		private Font(String s)
		{
			this.s = s;
		}
	}
	
	public enum PointSeqs
	{
		TRIANGLE(Arrays.asList(
				new Point(  0,  0), new Point(100,  0),   //line1
				new Point(100,  0), new Point( 50,100),   //line2
				new Point( 50,100), new Point(  0,  0))), //line3
		SQUARE(Arrays.asList(
				new Point(  0,  0), new Point(100,  0),   //line1
				new Point(100,  0), new Point(100,100),   //line2
				new Point(100,100), new Point(  0,100),   //line3
				new Point(  0,100), new Point(  0,  0))); //line4
		
		public final List<Point> s;
		
		private PointSeqs(List<Point> gobj)
		{
			this.s = gobj;
		}
	}
	
}

