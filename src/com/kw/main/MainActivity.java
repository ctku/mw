package com.kw.main;

import com.example.graphics.R;
import com.kw.main.GuiResource.Anm;
import com.kw.main.GuiResource.Gui;
import com.kw.mw.MW;
import com.kw.mw.graphics.G1;
import com.kw.mw.graphics.render.CanvasRenderer;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;


public class MainActivity extends Activity {

	private View renderCanvasView;
	private G1 v1;
	private Thread anmThread;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		renderCanvasView = initGui();
		setContentView(renderCanvasView);

		demoGuiStatic();
		demoGuiAnimation();
	}

	private View initGui()
	{
		v1 = new G1(this, new CanvasRenderer(this));
		v1.setBgColor(Color.BLACK);

		return v1.getRenderSurfaceView();
	}
	
	private void demoGuiStatic()
	{
		v1.showGobj(Gui.RECT__YELLOW_BODY_RED_EDGE_STROKE_10PXL.id);
		v1.showGobj(Gui.OVAL__CIRCLE_BLACK_BODY_WHITE_EDGE_STROKE5.id);
		v1.flushBuffer();
	}
	
	private void demoGuiAnimation()
	{
		// demo "Alpha" motion
		v1.setMotionParam(Anm.BMP_ALPHA.id, 0, 255, MW.NA, MW.NA); //a1,a2,NA,NA
		v1.anmPlayForward(Anm.BMP_ALPHA.id);
		
		// demo "Size" motion
		v1.setMotionParam(Anm.SQUARE_FLY.id, 0, 10, 800, 10); // x1,y1,x2,y2
		v1.anmPlayForward(Anm.SQUARE_FLY.id);
		
		// demo "Position motion
		v1.setMotionParam(Anm.BMP_FLY.id, 0, 50, 800, 50); // w1,h1,w2,h2
		v1.anmPlayForward(Anm.BMP_FLY.id);		
		
		/*
		// demo "an easy way to manipulate multiple animations"
		for (int i=0; i<5; i++)
		{
			v1.setMotionParam(Anms.BULLETS.id.get(i), 0, i*200, 800, i*200);
			v1.anmPlayForward(Anms.BULLETS.id.get(i));
		}
		for (int i=5; i<10; i++)
		{
			v1.setMotionParam(Anms.BULLETS.id.get(i), 800, (i-5)*200+100, 0, (i-5)*200+100);
			v1.anmPlayForward(Anms.BULLETS.id.get(i));
		}
		*/
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (event.getAction()==MotionEvent.ACTION_DOWN) {
			anmThread.start();
		}
			
		if (event.getAction()==MotionEvent.ACTION_UP) {
				;
		}
		return true;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
