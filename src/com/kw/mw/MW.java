package com.kw.mw;

public class MW {

	// used for "Not Apply" slot
	public static final int NA = -1; 

	// used for 4 params passing
	public class FourParams
	{
		public Object o1;
		public Object o2;
		public Object o3;
		public Object o4;
		
		public FourParams(Object o1, Object o2, Object o3, Object o4)
		{
			this.o1 = o1;
			this.o2 = o2;
			this.o3 = o3;
			this.o4 = o4;
		}
	}
	
}
