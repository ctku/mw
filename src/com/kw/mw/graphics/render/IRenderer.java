package com.kw.mw.graphics.render;

import com.kw.mw.graphics.gui.GuiElm;

import android.graphics.Rect;

public interface IRenderer 
{
	
	void renderBitmap(GuiElm gobj, Object o1, Object o2);
	
	void renderString(GuiElm gobj, Object o1, Object o2);
	
	void renderShape(GuiElm gobj, Object o1, Object o2);
	
	void flushBuffer(Rect invalid, Object o1);
	
}
