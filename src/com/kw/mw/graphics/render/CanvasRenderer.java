package com.kw.mw.graphics.render;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.View;

import com.kw.mw.MW;
import com.kw.mw.graphics.gui.GuiElm;
import com.kw.mw.graphics.gui.GuiElm.Scale;

public class CanvasRenderer extends Renderer
{
	private Context context;
	private RectF rect;
	private Bitmap bmp;
	private Matrix mtx;

	public CanvasRenderer(Context context)
	{
		this.context = context;
		rect = new RectF();
	    mtx = new Matrix();
	}
	
	@Override
	public void renderBitmap(GuiElm gobj, Object o1, Object o2) 
	{
		Canvas canvas = (Canvas)o1;
		Paint paint = new Paint();
		
		int rotate = gobj.strokeW;
		bmp = BitmapFactory.decodeResource(context.getResources(), gobj.resId);
		float src_w = bmp.getRowBytes()/4;
		float src_h = bmp.getHeight();
		float dst_w = gobj.w;
		float dst_h = gobj.h;
		float sw = dst_w/src_w;
		float sh = dst_h/src_h;
		
		// set alpha
		if (gobj.fclr != MW.NA)
			paint.setAlpha(gobj.fclr);
		
		// order for mtx setup is crucial
		mtx.reset();
		if (gobj.scale == Scale.FIT_RECT)
			mtx.postScale(sw, sh);
		if (rotate > 0)
			if (gobj.scale == Scale.FIT_RECT)
				mtx.postRotate(gobj.strokeW, dst_w/2f, dst_h/2f);
			else
				mtx.postRotate(gobj.strokeW, src_w/2f, src_h/2f);
		mtx.postTranslate(gobj.l, gobj.t);
		
		canvas.drawBitmap(bmp, mtx, paint);
	}

	@Override
	public void renderString(GuiElm gobj, Object o1, Object o2) 
	{
		Canvas canvas = (Canvas)o1;
		Paint paint = new Paint();
		paint.setTextSize(gobj.h);
		paint.setAntiAlias(true);
		
		// set font type
		if (!gobj.aux.toString().equals(MW.NA + ""))
		{
			//Typeface font = Typeface.create("SANS_SERIF"/*Typeface.SANS_SERIF*/, Typeface.NORMAL);
			Typeface font = Typeface.createFromAsset(context.getAssets(), gobj.aux.toString());
			paint.setTypeface(font);
		}
		
		// truncate string to fit gobj size
		String text = getTruncatedStringBySize(gobj, paint);
		
		// compensate internal bound to make it LEFT-TOP aligned
		Rect r = new Rect();
		paint.getTextBounds(text, 0, text.length(), r);
		int gobj_t = gobj.t - r.top + 1;
		int gobj_l = gobj.l - r.left + 1;

		// draw font outline
		float w = gobj.strokeW;
		if (w > 0)
		{
			paint.setColor(gobj.fclr);
			canvas.drawText(text, gobj_l-w, gobj_t-w, paint);//lt
			canvas.drawText(text, gobj_l-w, gobj_t  , paint);//lc
			canvas.drawText(text, gobj_l-w, gobj_t+w, paint);//lb
			canvas.drawText(text, gobj_l  , gobj_t-w, paint);//ct
			canvas.drawText(text, gobj_l  , gobj_t  , paint);//cc
			canvas.drawText(text, gobj_l  , gobj_t+w, paint);//cb
			canvas.drawText(text, gobj_l+w, gobj_t-w, paint);//rt
			canvas.drawText(text, gobj_l+w, gobj_t  , paint);//rc
			canvas.drawText(text, gobj_l+w, gobj_t+w, paint);//rb
		}
		// draw font body
		paint.setColor(gobj.bclr);
		canvas.drawText(text, gobj_l, gobj_t, paint);
	}

	@Override
	public void renderShape(GuiElm gobj, Object o1, Object o2) 
	{
		Canvas canvas = (Canvas)o1;
		Paint paint = new Paint();
		
		// set paint
		assert gobj.resId >= 0;
		paint.setStrokeWidth(gobj.strokeW);
		paint.setColor(gobj.fclr);
		//fpaint.setShadowLayer(10f, 10f, 10f, Color.BLACK);
		
		// set region
		rect.set(gobj.l, gobj.t, gobj.l+gobj.w-1, gobj.t+gobj.h-1);
		
		// render
		switch(gobj.attr)
		{
		case RECT:
			if (gobj.fclr == gobj.bclr)
			{
				// draw edge & body together
				paint.setStyle(Paint.Style.FILL);
				canvas.drawRect(rect, paint);
			}
			else
			{
				// draw edge
				paint.setStyle(Paint.Style.STROKE);
				canvas.drawRect(rect, paint);
				// draw body
				paint.setColor(gobj.bclr);
				paint.setStyle(Paint.Style.FILL);
				canvas.drawRect(rect, paint);
			}
			break;
		case OVAL:
			if (gobj.fclr == gobj.bclr)
			{
				// draw edge & body together
				paint.setStyle(Paint.Style.FILL_AND_STROKE);
				canvas.drawOval(rect, paint);
			}
			else
			{
				// draw edge
				paint.setStyle(Paint.Style.STROKE);
				canvas.drawOval(rect, paint);
				// draw body
				paint.setColor(gobj.bclr);
				paint.setStyle(Paint.Style.FILL);
				canvas.drawOval(rect, paint);
			}
			break;
		case LINES:
			@SuppressWarnings("unchecked")
			List<Point> pts = (List<Point>)(gobj.aux);
			
			float[] points = new float[pts.size()*2];
			int idx = 0;
			for (Point pt : pts)
			{
				points[idx] = pt.x + gobj.l; idx ++;
				points[idx] = pt.y + gobj.t; idx ++;
			}
			canvas.drawLines(points, paint);
			break;
		default:
		}	
	}

	@Override
	public void flushBuffer(Rect invalid, Object o1) 
	{
		((View)o1).invalidate(invalid);
	}
	
	private String getTruncatedStringBySize(GuiElm gobj, Paint paint)
	{
		String text = context.getString(gobj.resId) + " ";
		Rect r = new Rect();
		int w;
		do 
		{
			text = text.substring(0, text.length()-1);
			paint.getTextBounds(text, 0, text.length(), r);
			w = (r.right - r.left - 1);
		} 
		while (gobj.w < w);
		
		return text;
	}
	
}
