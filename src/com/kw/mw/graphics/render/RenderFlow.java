package com.kw.mw.graphics.render;

import java.util.ArrayList;

import com.kw.mw.graphics.gui.GuiElm;
import com.kw.mw.graphics.gui.GuiFlow;
import com.kw.mw.graphics.gui.GuiPlane;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

public class RenderFlow
{
	private static final int DEFAULT_BGCOLOR = Color.BLACK;
	
	private View view;
	private ArrayList<GuiPlane> gps;
	private Renderer renderer;
	private Rect invalid;
	private int bgcolor;
	private int draw_pln;
	private int draw_pos;
	
	public RenderFlow(Context context, Renderer renderer)
	{
		view = new RenderSurfaceView(context);
		this.renderer = renderer;
		invalid = new Rect(0, 0, 0, 0);
		bgcolor = DEFAULT_BGCOLOR;
		draw_pln = GuiFlow.MAX_GUI_PLANES;
		draw_pos = GuiFlow.MAX_GUI_OBJS_PER_PLANE;
	}
	
	public void setGplanes(ArrayList<GuiPlane> gps)
	{
		this.gps = gps;
	}

	public View getRenderSurfaceView()
	{
		return view;
	}
	
	public void setBgColor(int bgcolor)
	{
		if (this.bgcolor == bgcolor) return;
		this.bgcolor = bgcolor;
		updateWholeView(0, 0);
	}

	public void updatePartialView(Rect rect, int gp, int pos)
	{
		invalidateRegion(rect, gp, pos);
	}
	
	public void updateWholeView(int gp, int pos)
	{
		invalidateRegion(new Rect(0, 0, view.getWidth(), view.getHeight()), gp, pos);
	}
	
	private void invalidateRegion(Rect rect, int targ_pln, int targ_pos)
	{
		invalid.union(rect);
		
		if (draw_pln > targ_pln) 
		{
			draw_pln = targ_pln;
			draw_pos = targ_pos;
		} 
		else if (draw_pln == targ_pln) 
		{
			if (draw_pos > targ_pos)
				draw_pos = targ_pos;
		}
	}

	public void flushBuffer()
	{
		if (invalid.width()==0 && invalid.height()==0) return;
		renderer.flushBuffer(invalid, view);
	}

	public class RenderSurfaceView extends View
	{
		private Paint paint;
		
		public RenderSurfaceView(Context context) 
		{
			super(context);
			paint = new Paint();
		}
		
		protected void onDraw(Canvas canvas)
		{
			canvas.drawColor(bgcolor);
			
			for (int i = draw_pln; i<gps.size(); i++)
			{
				GuiPlane gp = gps.get(i);
				if (!gp.isVisible()) continue;
				
				for (int j = draw_pos; j<gp.getAllGobjs().size(); j++)
				{
					GuiElm gobj = gp.getAllGobjs().get(j);
					drawGobj(gobj, canvas);
				}
			}
			
			// reset some values after drawing
			invalid.set(0, 0, 0, 0);
			draw_pln = GuiFlow.MAX_GUI_PLANES;
			draw_pos = GuiFlow.MAX_GUI_OBJS_PER_PLANE;
		}

		private void drawGobj(GuiElm gobj, Canvas canvas)
		{
			switch (gobj.type)
			{
			case BITMAP:
				renderer.renderBitmap(gobj, canvas, paint);
				break;
			case STRING:
				renderer.renderString(gobj, canvas, paint);
				break;
			case SHAPE:
				renderer.renderShape(gobj, canvas, paint);
				break;
			default:
			}
		}
	}
	
}
