package com.kw.mw.graphics.gui;

import java.util.List;

import android.graphics.Point;
import android.graphics.Rect;

public class GuiElm 
{
	public Type type;     // string, bitmap, shape id, or layer id (see Type enum)
	public Attr attr;     // attribute (see Attr enum)
	public Object aux;    // auxiliary (string:either given font path(custom font) or leave NA(default font))
	public int fclr;      // foregraound color (shape:edge; string:textoutline, bmp:alpha value)
	public int bclr;      // background color (shape:body; string:textbody)
	public Scale scale;   // scale mode (see Scale enum)
	public int strokeW;   // stroke width (shape:edge width; string:stroke width; bitmap:rotate angle)
	public int resId;     // resource ID (bitmap/string:resource id)
	public int gp;        // graphics plane
	public int l;         // left position of the element
	public int t;         // top position of the element
	public int w;         // width of the element
	public int h;         // height of the element
	
	private int[] flags = new int[1];
	private static final int ADDED = 0;

	public GuiElm(Type type, Attr attr, Object aux, int fclr, int bclr, Scale scale, int strokeW, int gp, int l, int t, int w, int h, int resId) 
	{
		this.type = type;
		this.attr = attr;
		this.aux = aux;
		this.fclr = fclr;
		this.bclr = bclr;
		this.strokeW = strokeW;
		this.scale = scale;
		this.gp = gp;
		this.l = l;
		this.t = t;
		this.w = w;
		this.h = h;
		this.resId = resId;
		this.flags[0] = 0;
	}
	
	public static enum Type 
	{
		STRING,
		BITMAP,
		SHAPE;
	}
	
	public static enum Attr 
	{
		// for string
		NORMAL,

		// for bitmap
		STATIC,
		
		// for shape
		RECT,
		OVAL,
		LINES;
	}
	
	public static enum Scale 
	{
		// for string (N/A)

		// for bitmap
		NONE,
		FIT_RECT;
		
		// for shape (N/A)
	}

	public void addOnGP() 
	{
		this.flags[ADDED] = 1;
	}
	
	public void removeOnGP() 
	{
		this.flags[ADDED] = 0;
	}
	
	public boolean isAddedOnGP() 
	{
		return (this.flags[ADDED] > 0);
	}

	public Rect getSizeRect() 
	{
		Rect rect = new Rect();
		
		switch(this.type)
		{
		case STRING:
		case BITMAP:
			rect.set(this.l, this.t, this.l+this.w-1, this.t+this.h-1);
			break;
		case SHAPE:
			int s = this.strokeW;
			switch(this.attr)
			{
			case RECT:
			case OVAL:
				rect.set(this.l-s, this.t-s, this.l+this.w-1+s, this.t+this.h-1+s);
				break;
			case LINES:
				@SuppressWarnings("unchecked")
				List<Point> pts = (List<Point>)(this.aux);
				int l=9999, t=9999, r=0, b=0;
				for (Point p : pts)
				{
					l = Math.min(l, p.x);
					t = Math.min(t, p.y);
					r = Math.max(r, p.x);
					b = Math.max(b, p.y);
				}
				rect.set(this.l+l-s, this.t+t-s, this.l+r+s, this.t+b+s);	
				break;
			default:
			}
			break;
		default:
		}
		
		return rect;
	}
	
}
