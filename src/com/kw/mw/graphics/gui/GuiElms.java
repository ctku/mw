package com.kw.mw.graphics.gui;

import java.util.ArrayList;

import com.kw.mw.graphics.gui.GuiElm.Attr;
import com.kw.mw.graphics.gui.GuiElm.Scale;
import com.kw.mw.graphics.gui.GuiElm.Type;

public class GuiElms {

	private ArrayList<GuiElm> gobjs = new ArrayList<GuiElm>();
	
	public GuiElms(int num, Type type, Attr attr, Object aux, int fclr, int bclr, Scale scale, int strokeW, int gp, int l, int t, int w, int h,	int resId)
	{
		for (int i=0; i<num; i++)
		{
			GuiElm gobj = new GuiElm(type, attr, aux, fclr, bclr, scale, strokeW, gp, l, t, w, h, resId);
			gobjs.add(gobj);
		}
	}
	
	public ArrayList<GuiElm> get()
	{
		return gobjs;
	}
}
