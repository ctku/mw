package com.kw.mw.graphics.gui;

import java.util.ArrayList;

import com.kw.mw.graphics.render.RenderFlow;

public class GuiFlow
{
	public static final int MAX_GUI_PLANES = 16;
	public static final int MAX_GUI_OBJS_PER_PLANE = 64;
	private ArrayList<GuiPlane> gps;
	private RenderFlow renderFlow;
	
	public GuiFlow() 
	{
		// get gplanes ready
		gps = new ArrayList<GuiPlane>();
		for (int i=0; i<MAX_GUI_PLANES; i++)
			gps.add(new GuiPlane());
	}
	
	public void setRenderFlow(RenderFlow renderFlow)
	{
		this.renderFlow = renderFlow;
	}
	
	public ArrayList<GuiPlane> getGplanes()
	{
		return gps;
	}

	public boolean addGobjOnAGplane(GuiElm gobj, int gp)
	{
		if (!gobj.isAddedOnGP()) 
		{
			GuiPlane cur_gp = gps.get(gp);
			gobj.addOnGP();
			cur_gp.addGobj(gobj);
			assert cur_gp.getNumOfGobjs() < MAX_GUI_OBJS_PER_PLANE;
			renderFlow.updatePartialView(gobj.getSizeRect(), gp, cur_gp.getGobjIdx(gobj));
			
			return true;
		} else {
			return false;
		}
	}
	
	public boolean removeGobjOnAGplane(GuiElm gobj, int gp)
	{
		if (gobj.isAddedOnGP()) 
		{
			GuiPlane cur_gp = gps.get(gp);
			gobj.removeOnGP();
			cur_gp.removeGobj(gobj);
			renderFlow.updatePartialView(gobj.getSizeRect(), 0, 0);
			
			return true;
		} else {
			return false;
		}
	}
	
	public boolean removeAllGobjsOnAGplane(int gp)
	{
		boolean rval = false;
		
		ArrayList<GuiElm> gobjs = gps.get(gp).getAllGobjs();
		for (int i = 0; i < gobjs.size(); i++)
			rval = removeGobjOnAGplane(gobjs.get(i), gp);
		
		// updatePartialView is called in removeGobjOnAGplane
		
		return rval;
	}
	
	public boolean removeAllGobjsOnAllGplanes()
	{
		boolean rval = false;
		
		for (int i = 0; i < gps.size(); i++)
			rval = removeAllGobjsOnAGplane(i);
		
		// updatePartialView is called in removeAllGobjsOnAGplane
		
		return rval;
	}
	
	public void makeGplaneInvisible(int gp)
	{
		GuiPlane cur_gp = gps.get(gp);
		cur_gp.makeItInvisible();
		
		ArrayList<GuiElm> gobjs = gps.get(gp).getAllGobjs();
		for (int i = 0; i < gobjs.size(); i++)
		{
			GuiElm gobj = gobjs.get(i);
			renderFlow.updatePartialView(gobj.getSizeRect(), 0, 0);
		}
	}

	public void makeGplaneVisible(int gp)
	{
		GuiPlane cur_gp = gps.get(gp);
		cur_gp.makeItVisible();
		
		ArrayList<GuiElm> gobjs = gps.get(gp).getAllGobjs();
		for (int i = 0; i < gobjs.size(); i++)
		{
			GuiElm gobj = gobjs.get(i);
			renderFlow.updatePartialView(gobj.getSizeRect(),0, 0);
		}
	}
	
	public void updateSize(GuiElm gobj, int w, int h)
	{
		removeGobjOnAGplane(gobj, gobj.gp);
		
		gobj.w = w;
		gobj.h = h;
		
		addGobjOnAGplane(gobj, gobj.gp);
	}
	
	public void updatePos(GuiElm gobj, int l, int t)
	{
		removeGobjOnAGplane(gobj, gobj.gp);
		
		gobj.l = l;
		gobj.t = t;
		
		addGobjOnAGplane(gobj, gobj.gp);
	}
	
	public void updateColor(GuiElm gobj, int fclr, int bclr)
	{
		gobj.fclr = fclr;
		gobj.bclr = bclr;
		
		renderFlow.updatePartialView(gobj.getSizeRect(), 0, 0);
	}
}
