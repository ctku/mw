package com.kw.mw.graphics.gui;

import java.util.ArrayList;

public class GuiPlane 
{
	private static final boolean DEFAULT_VISIBLE = true;
	
	private boolean visible;         // display or not
	private ArrayList<GuiElm> gobjs; // store GUI object ID (no: max_gobj)
	
	public GuiPlane()
	{
		gobjs = new ArrayList<GuiElm>();
		visible = DEFAULT_VISIBLE;
	}
	
	public void addGobj(GuiElm gobj) 
	{
		gobjs.add(gobj);
	}
	
	public void removeGobj(GuiElm gobj)
	{
		gobjs.remove(gobj);
	}
	
	public int getNumOfGobjs()
	{
		return gobjs.size();
	}
	
	public void clearAllGobjs()
	{
		gobjs.clear();
	}
	
	public ArrayList<GuiElm> getAllGobjs()
	{
		return gobjs;
	}
	
	public int getGobjIdx(GuiElm gobj)
	{
		return gobjs.indexOf(gobj);
	}
	
	public void makeItVisible() 
	{
		visible = true;
	}
	
	public void makeItInvisible() 
	{
		visible = false;
	}
	
	public boolean isVisible() 
	{
		return visible;
	}
	
}
