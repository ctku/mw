package com.kw.mw.graphics.animator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;

import com.kw.mw.MW;
import com.kw.mw.graphics.animator.AnmElm;
import com.kw.mw.graphics.animator.AnmElm.State;
import com.kw.mw.graphics.gui.GuiElm;

public class AnmFlow 
{
	public static final int INITIAL_TIMER_DELAY = 300;
	public static final int MAX_ANM_NO = 16;
	private static AnmCallback anmCallback;
	public interface AnmCallback
	{ 
		void func_showGobj(GuiElm gobj);
		void func_hideGobj(GuiElm gobj);
		void func_updateSize(GuiElm gobj, int w, int h);
		void func_updatePos(GuiElm gobj, int l, int t);
		void func_updateColor(GuiElm gobj, int fclr, int bclr);
		void func_flushBuffer();
	}

	private ArrayList<AnmElm> anms;
	private static Handler uiThreadHandler = null;
	private Timer timerThread = null;
	private Boolean timerIsEnabled;
	
	public AnmFlow()
	{
		anms = new ArrayList<AnmElm>();

		if (uiThreadHandler == null)
			initUiThreadHandler();
		
		if (timerThread == null)
		{
			timerIsEnabled = false;
			initTimerThread();
		}	
	}
	
	public void setCallback(AnmCallback anmCallback)
	{
		AnmFlow.anmCallback = anmCallback;
	}
	
	public void anmUpdate()
	{
		for (AnmElm anm : anms)
		{
			if (anm.getState() != State.PLAY)
				continue;
			
			// clear animation info
			anm.getAnmMotionInfo().clearAnmMotionInfo();
			
			// callback motion handler
			anm.runMotion();
			
			// callback gui update handler (cause invalid change)
			anm.updateCorrespondingGui(anmCallback);
			
			// adjust frame to be displayed
			anm.adjustFrameToDisplay();
		}
	}
	
	public void setMotionParam(AnmElm anm, MW.FourParams p4)
	{
		if(!anms.contains(anm))
			anms.add(anm);
		
		anm.setMotionParam(p4);
		anm.setState(State.READY);
	}
	
	public enum PlayCmd
	{
		PLAY_FORWARD,
		PLAY_BACKWARD,
		STOP;
	}
	
	public void issuePlayCmd(AnmElm anm, PlayCmd cmd)
	{
		if (anm.getState() != State.NOT_READY)
		{
			switch (cmd)
			{
			case PLAY_FORWARD:
				anm.setFrmToDisp(0);
				anm.setState(State.PLAY);
				break;
			case PLAY_BACKWARD:
				anm.setFrmToDisp(anm.getFrmNo()-1);
				anm.setState(State.PLAY);
				break;
			case STOP:
				anm.setState(State.STOP);
				break;
			default:
			}
			anm.setPlayCmd(cmd);
		}
		else
			System.out.println("Cannot play/stop animation before setMotionParam!");
		
		adjustAnmTimer();
	}
	
	private void adjustAnmTimer()
	{
		for (AnmElm anm : anms)
		{
			if (anm.getState() == State.PLAY)
			{
				timerIsEnabled = true; 
				break;
			}
		}
	}
	
	@SuppressLint("HandlerLeak")
	private void initUiThreadHandler()
	{
		uiThreadHandler = new Handler()
		{
			@Override
			public void handleMessage(Message msg) {				
				super.handleMessage(msg);
				anmCallback.func_flushBuffer();
			}
		};
	}
	
	private void initTimerThread()
	{
		timerThread = new Timer();
        TimerTask anmTimerTask = new TimerTask() {
            @Override
            public void run() {
            	if (timerIsEnabled)
            		uiThreadHandler.sendEmptyMessage(0);
            }
        };
        timerThread.schedule(anmTimerTask, INITIAL_TIMER_DELAY, AnmElm.FRAME_TIME_UNIT);
	}
}
