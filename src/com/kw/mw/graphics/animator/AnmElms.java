package com.kw.mw.graphics.animator;

import java.util.ArrayList;

import com.kw.mw.graphics.animator.AnmElm.EndsBehavior;
import com.kw.mw.graphics.animator.AnmElm.Motion;
import com.kw.mw.graphics.animator.AnmElm.Target;
import com.kw.mw.graphics.gui.GuiElm;

public class AnmElms {

	private ArrayList<AnmElm> anms = new ArrayList<AnmElm>();
	
	public AnmElms(int num, ArrayList<GuiElm> gobjs, Target target, Motion motion, EndsBehavior ends, int loopNo, int duration)
	{
		assert num == gobjs.size();
		
		for (int i=0; i<num; i++)
		{
			AnmElm anm = new AnmElm(gobjs.get(i), target, motion, ends, loopNo, duration);
			anms.add(anm);
		}
	}
	
	public ArrayList<AnmElm> get()
	{
		return anms;
	}
}

