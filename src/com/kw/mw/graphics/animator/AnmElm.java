package com.kw.mw.graphics.animator;

import com.kw.mw.MW;
import com.kw.mw.graphics.animator.AnmFlow.AnmCallback;
import com.kw.mw.graphics.animator.AnmFlow.PlayCmd;
import com.kw.mw.graphics.animator.motion.AnmMotion;
import com.kw.mw.graphics.animator.motion.AnmMotionAccelerate;
import com.kw.mw.graphics.animator.motion.AnmMotionLinear;
import com.kw.mw.graphics.animator.motion.AnmMotionLinearLinear;
import com.kw.mw.graphics.animator.target.AnmTarget;
import com.kw.mw.graphics.animator.target.AnmTargetAlpha;
import com.kw.mw.graphics.animator.target.AnmTargetPosition;
import com.kw.mw.graphics.animator.target.AnmTargetSize;
import com.kw.mw.graphics.gui.GuiElm;
import com.kw.mw.graphics.gui.GuiElm.Type;

public class AnmElm 
{
	public static final int LOOP_INFINITE = 0;
	public static final int FRAME_TIME_UNIT = 100;
	
	private GuiElm gobj;           // associated GUI object
	private int loopNo;            // loop number
	private int frmNo;             // total frame number in each loop
	private int frmToDisp;         // frame to be displayed
	private int loopCur;           // current loop
	private AnmMotionInfo info;    // animation motion info
	private Target target;         // animation target
	private Motion motion;         // animation motion
	private State state;           // current state
	private EndsBehavior ends;     // ends behavior
	private PlayCmd playCmd;       // last play command
	
	private AnmTarget anmTarget = null;
	private AnmMotion anmMotion = null;
	
	private static AnmTarget anmTargetSize = new AnmTargetSize();
	private static AnmTarget anmTargetPosition = new AnmTargetPosition();
	private static AnmTarget anmTargetAlpha = new AnmTargetAlpha();
	
	private static AnmMotion anmMotionLinear = new AnmMotionLinear();
	private static AnmMotion anmMotionAccelerate = new AnmMotionAccelerate();
	private static AnmMotion anmMotionLinearLinear = new AnmMotionLinearLinear();
	
	public AnmElm(GuiElm gobj, Target target, Motion motion, EndsBehavior ends, int loopNo, int duration)
	{
		this.gobj = gobj;
		this.target = target;
		this.motion = motion;
		this.ends = ends;
		this.loopNo = loopNo;
		
		if (duration % FRAME_TIME_UNIT > 0)
			this.frmNo = (duration / FRAME_TIME_UNIT) + 1;
		else
			this.frmNo = duration / FRAME_TIME_UNIT;

		frmToDisp = 0;
		info = new AnmMotionInfo();
		state = State.NOT_READY;
	}

	public void setMotionParam(MW.FourParams p4)
	{
		if (target == Target.USER_DEFINED)
			anmTarget = (AnmTarget)p4.o1;

		info.setParam(p4);
	}

	public void adjustFrameToDisplay()
	{
		switch (playCmd)
		{
		case PLAY_FORWARD:

			state = State.PLAY;
			
			// check loop num
			if (loopNo == LOOP_INFINITE)
			{
				frmToDisp ++;
				
				// reach end of a loop
				if (frmToDisp == frmNo)
				{
					loopCur ++;
					frmToDisp = 0;
				}
			}
			else if (loopCur < loopNo)
			{
				frmToDisp ++;
				
				// reach end of a loop
				if (frmToDisp == frmNo)
				{
					loopCur ++;
					if (loopCur < loopNo)
						// not last loop
						frmToDisp = 0;
					else
					{
						// reach last loop
						frmToDisp = frmNo - 1;
						loopCur = 0;
						state = State.END;
					}
				}
			}
			else
				// exceed loop no
				state = State.STOP;

			break;
			
		case PLAY_BACKWARD:
			
			state = State.PLAY;
			
			// check loop num
			if (loopNo == LOOP_INFINITE)
			{
				frmToDisp --;
				
				// reach end of a loop
				if (frmToDisp == -1) 
				{
					loopCur ++;
					frmToDisp = frmNo - 1;
				}
			}
			else if (loopCur < loopNo)
			{
				frmToDisp --;
				
				// reach end of a loop
				if (frmToDisp == -1)
				{
					loopCur ++;
					if (loopCur < loopNo)
						// not last loop
						frmToDisp = frmNo - 1;
					else
					{
						// reach last loop
						frmToDisp = 0;
						loopCur = 0;
						state = State.END;
					}
				}
			}
			else
				// exceed loop no
				state = State.STOP;
			
			break;
		default:	
		}
	}
	
	public void runMotion()
	{
		switch(motion)
		{
		case LINEAR:
			anmMotion = (AnmMotion)anmMotionLinear;
			break;
		case ACCELERATE:
			anmMotion = (AnmMotion)anmMotionAccelerate;
			break;
		case LINEAR_LINEAR:
			anmMotion = (AnmMotion)anmMotionLinearLinear;
		default:
		}
		
		switch(target)
		{
		case SIZE:
			anmTarget = (AnmTarget)anmTargetSize;
			break;
		case POSITION:
			anmTarget = (AnmTarget)anmTargetPosition;
			break;
		case ALPHA:
			anmTarget = (AnmTarget)anmTargetAlpha;
			break;
		case USER_DEFINED:
			assert(anmTarget != null);
			break;
		default:
		}
		
		anmTarget.updateInfo(this, info);
	}
	
	public void updateCorrespondingGui(AnmCallback anmCallback)
	{
		if ((state == State.PLAY) &&
			((ends == EndsBehavior.AUTO_SHOW) ||
			 (ends == EndsBehavior.AUTO_SHOW_HIDE)))
			anmCallback.func_showGobj(gobj);
		
		if ((info.w != AnmMotionInfo.NO_CHANGE) ||
				(info.h != AnmMotionInfo.NO_CHANGE))
				anmCallback.func_updateSize(gobj, info.w, info.h);
		
		if ((info.l != AnmMotionInfo.NO_CHANGE) ||
			(info.t != AnmMotionInfo.NO_CHANGE))
			anmCallback.func_updatePos(gobj, info.l, info.t);
		
		if ((info.alpha != AnmMotionInfo.NO_CHANGE) &&
		    (gobj.type == Type.BITMAP))
			anmCallback.func_updateColor(gobj, info.alpha, MW.NA);

		if ((state == State.END) &&
			((ends == EndsBehavior.AUTO_HIDE) ||
			 (ends == EndsBehavior.AUTO_SHOW_HIDE)))
			anmCallback.func_hideGobj(gobj);
	}
	
	public GuiElm getGobj()
	{
		return gobj;
	}
	
	public int getFrmNo()
	{
		return frmNo;
	}

	public void setFrmToDisp(int frm)
	{
		frmToDisp = frm;
	}

	public int getFrmToDisp()
	{
		return frmToDisp;
	}
	
	public State getState()
	{
		return state;
	}
	
	public void setState(State state)
	{
		this.state = state;
	}
	
	public void setPlayCmd(PlayCmd cmd)
	{
		playCmd = cmd;
	}
	
	public PlayCmd getPlayCmd()
	{
		return playCmd;
	}
	
	public AnmMotionInfo getAnmMotionInfo()
	{
		return info;
	}
	
	public AnmMotion getAnmMotion()
	{
		return anmMotion;
	}
	
	public AnmTarget getAnmTarget()
	{
		return anmTarget;
	}
	
	public static enum Target
	{
		POSITION,
		SIZE,
		ALPHA,
		USER_DEFINED;
	}
	
	public static enum Motion 
	{
		LINEAR,          //one way :(go)linear
		ACCELERATE,      //one way :(go)accelerate
		LINEAR_LINEAR;   //two ways:(go)linear+(back)linear
	}

	public static enum State 
	{
		NOT_READY,   // before animation info is set
		READY,       // after animation info is set
		STOP,        // when it is stopped
		PLAY,        // when it is played
		END;         // when it finishes automatically
	}
	
	public static enum EndsBehavior
	{
		NONE,           // do nothing, user has to showGobj & hideGobj by his own
		AUTO_SHOW,      // auto showGobj when play
		AUTO_HIDE,      // auto hideGobj when end
		AUTO_SHOW_HIDE; // auto showGobj when play & auto hideGobj when end
	}
}
