package com.kw.mw.graphics.animator.target;

import com.kw.mw.graphics.animator.AnmElm;
import com.kw.mw.graphics.animator.AnmMotionInfo;

public class AnmTargetSize implements AnmTarget
{
	public void updateInfo(AnmElm anm, AnmMotionInfo info)
	{
		int w1 = Integer.parseInt(info.o1.toString());
		int h1 = Integer.parseInt(info.o2.toString());
		int w2 = Integer.parseInt(info.o3.toString());
		int h2 = Integer.parseInt(info.o4.toString());
		int frmToDisp = (int)(anm.getAnmMotion().getInterpolation(anm.getFrmToDisp()*1f/anm.getFrmNo())*anm.getFrmNo());
		float dw = ((w2 - w1) * 1f) / anm.getFrmNo();
		float dh = ((h2 - h1) * 1f) / anm.getFrmNo();
		info.w = (int)(w1 + (frmToDisp * dw));
		info.h = (int)(h1 + (frmToDisp * dh));
	}
}

