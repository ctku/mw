package com.kw.mw.graphics.animator.target;

import com.kw.mw.graphics.animator.AnmElm;
import com.kw.mw.graphics.animator.AnmMotionInfo;

public interface AnmTarget 
{
	public void updateInfo(AnmElm anm, AnmMotionInfo info);
}
