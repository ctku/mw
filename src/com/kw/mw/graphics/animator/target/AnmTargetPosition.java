package com.kw.mw.graphics.animator.target;

import android.util.Log;

import com.kw.mw.graphics.animator.AnmElm;
import com.kw.mw.graphics.animator.AnmMotionInfo;

public class AnmTargetPosition implements AnmTarget
{
	public void updateInfo(AnmElm anm, AnmMotionInfo info)
	{
		int x1 = Integer.parseInt(info.o1.toString());
		int y1 = Integer.parseInt(info.o2.toString());
		int x2 = Integer.parseInt(info.o3.toString());
		int y2 = Integer.parseInt(info.o4.toString());
		float dx = ((x2 - x1) * 1f) / anm.getFrmNo();
		float dy = ((y2 - y1) * 1f) / anm.getFrmNo();
		int frmToDisp = (int)(anm.getAnmMotion().getInterpolation(anm.getFrmToDisp()*1f/anm.getFrmNo())*anm.getFrmNo());
		info.l = (int)(x1 + (frmToDisp * dx));
		info.t = (int)(y1 + (frmToDisp * dy));
		Log.d("L","["+System.currentTimeMillis()+"] "+info.l); 
	}
}
