package com.kw.mw.graphics.animator.target;

import com.kw.mw.graphics.animator.AnmElm;
import com.kw.mw.graphics.animator.AnmMotionInfo;

public class AnmTargetAlpha implements AnmTarget
{
	public void updateInfo(AnmElm anm, AnmMotionInfo info)
	{
		int a1 = Integer.parseInt(info.o1.toString());
		int a2 = Integer.parseInt(info.o2.toString());
		float da = ((a2 - a1) * 1f) / anm.getFrmNo();
		int frmToDisp = (int)(anm.getAnmMotion().getInterpolation(anm.getFrmToDisp()*1f/anm.getFrmNo())*anm.getFrmNo());
		info.alpha = (int)(a1 + (frmToDisp * da));
	}
}


