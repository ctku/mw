package com.kw.mw.graphics.animator.motion;

import android.view.animation.LinearInterpolator;

public class AnmMotionLinearLinear implements AnmMotion 
{
	public float getInterpolation(float input) 
	{
		LinearInterpolator linear = new LinearInterpolator();
		if (input <= 0.5)
			return linear.getInterpolation(input)*2;
		else
			return (1-linear.getInterpolation(input))*2;
	}
}
