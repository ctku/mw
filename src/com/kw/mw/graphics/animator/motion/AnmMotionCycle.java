package com.kw.mw.graphics.animator.motion;

import android.view.animation.CycleInterpolator;

public class AnmMotionCycle implements AnmMotion 
{
	public float getInterpolation(float input) 
	{
		CycleInterpolator linear = new CycleInterpolator(1);
		
		return linear.getInterpolation(input);
	}
}