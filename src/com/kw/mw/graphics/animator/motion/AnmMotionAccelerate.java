package com.kw.mw.graphics.animator.motion;

import android.view.animation.AccelerateInterpolator;

public class AnmMotionAccelerate implements AnmMotion 
{
	public float getInterpolation(float input) 
	{
		AccelerateInterpolator accelerate = new AccelerateInterpolator();
		
		return accelerate.getInterpolation(input);
	}
}
