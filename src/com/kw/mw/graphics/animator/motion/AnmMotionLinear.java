package com.kw.mw.graphics.animator.motion;

import android.view.animation.LinearInterpolator;

public class AnmMotionLinear implements AnmMotion 
{
	public float getInterpolation(float input) 
	{
		LinearInterpolator linear = new LinearInterpolator();
		
		return linear.getInterpolation(input);
	}
}
