package com.kw.mw.graphics.animator.motion;

public interface AnmMotion 
{
	public float getInterpolation(float input);
}
