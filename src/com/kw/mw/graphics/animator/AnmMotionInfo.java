package com.kw.mw.graphics.animator;

import com.kw.mw.MW;

public class AnmMotionInfo {

	public static final int NO_CHANGE = -1;

	public int l;
	public int t;
	public int w;
	public int h;
	public int alpha;
	public Object o1;
	public Object o2;
	public Object o3;
	public Object o4;

	public void clearAnmMotionInfo()
	{
		l = NO_CHANGE;
		t = NO_CHANGE;
		w = NO_CHANGE;
		h = NO_CHANGE;
		alpha = NO_CHANGE;
	}
	
	public void setParam(MW.FourParams p4)
	{
		o1 = p4.o1;
		o2 = p4.o2;
		o3 = p4.o3;
		o4 = p4.o4;
	}
}
