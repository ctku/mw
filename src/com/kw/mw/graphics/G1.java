package com.kw.mw.graphics;

import android.content.Context;
import android.view.View;

import com.kw.mw.MW;
import com.kw.mw.graphics.animator.AnmElm;
import com.kw.mw.graphics.animator.AnmFlow;
import com.kw.mw.graphics.animator.AnmFlow.AnmCallback;
import com.kw.mw.graphics.animator.AnmFlow.PlayCmd;
import com.kw.mw.graphics.gui.GuiElm;
import com.kw.mw.graphics.gui.GuiFlow;
import com.kw.mw.graphics.render.RenderFlow;
import com.kw.mw.graphics.render.Renderer;

public class G1 
{
	private RenderFlow renderFlow;
	private GuiFlow guiFlow;
	private AnmFlow anmFlow;
	
	public G1(Context context, Renderer renderer)
	{
		// init render flow
		renderFlow = new RenderFlow(context, renderer);
		
		// init gui flow
		guiFlow = new GuiFlow();
		renderFlow.setGplanes(guiFlow.getGplanes());
		guiFlow.setRenderFlow(renderFlow);
		
		// init anm flow
		anmFlow = new AnmFlow();
		AnmFlow.AnmCallback anmCallback = new AnmCallback() 
		{ 
			public void func_showGobj(GuiElm gobj) { showGobj(gobj); }
			public void func_hideGobj(GuiElm gobj) { hideGobj(gobj); }
			public void func_updateSize(GuiElm gobj, int w, int h) { updateSize(gobj, w, h); }
			public void func_updatePos(GuiElm gobj, int l, int t) { updatePos(gobj, l, t); }
			public void func_updateColor(GuiElm gobj, int fclr, int bclr) { updateColor(gobj, fclr, bclr); }
			public void func_flushBuffer() { flushBuffer(); }
		};
		anmFlow.setCallback(anmCallback);
	}
	
	public View getRenderSurfaceView()
	{
		return renderFlow.getRenderSurfaceView();
	}
	
	public void showGobj(GuiElm gobj)
	{
		guiFlow.addGobjOnAGplane(gobj, gobj.gp);
	}
	
	public void hideGobj(GuiElm gobj)
	{
		guiFlow.removeGobjOnAGplane(gobj, gobj.gp);
	}
	
	public void hideAll()
	{
		guiFlow.removeAllGobjsOnAllGplanes();
	}
	
	public void hideGplane(int gp)
	{
		guiFlow.removeAllGobjsOnAGplane(gp);
	}
	
	public void concealGplane(int gp)
	{
		guiFlow.makeGplaneInvisible(gp);
	}
	
	public void revealGplane(int gp)
	{
		guiFlow.makeGplaneVisible(gp);
	}
	
	public void updateSize(GuiElm gobj, int w, int h)
	{
		guiFlow.updateSize(gobj, w, h);
	}
	
	public void updatePos(GuiElm gobj, int l, int t)
	{
		guiFlow.updatePos(gobj, l, t);
	}
	
	public void updateColor(GuiElm gobj, int fclr, int bclr)
	{
		guiFlow.updateColor(gobj, fclr, bclr);
	}
	
	public void flushBuffer()
	{
		anmFlow.anmUpdate();
		renderFlow.flushBuffer();
	}
	
	public void setBgColor(int color)
	{
		renderFlow.setBgColor(color);
	}
	
	public void setMotionParam(AnmElm anm, Object o1, Object o2, Object o3, Object o4)
	{
		MW.FourParams p4 = (new MW()).new FourParams(o1, o2, o3, o4);
		anmFlow.setMotionParam(anm, p4);
	}

	public void anmPlayForward(AnmElm anm)
	{
		anmFlow.issuePlayCmd(anm, PlayCmd.PLAY_FORWARD);
	}
	
	public void anmPlayBackward(AnmElm anm)
	{
		anmFlow.issuePlayCmd(anm, PlayCmd.PLAY_BACKWARD);
	}
}
